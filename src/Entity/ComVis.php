<?php

namespace App\Entity;

use App\Repository\ComVisRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ComVisRepository::class)]
class ComVis
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $vis;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVis(): ?int
    {
        return $this->vis;
    }

    public function setVis(int $vis): self
    {
        $this->vis = $vis;

        return $this;
    }
}
