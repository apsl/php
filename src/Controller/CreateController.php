<?php

namespace App\Controller;


use App\Entity\Articles;
use App\Entity\Categories;
use App\Entity\Comment;
use App\Form\ArticlesFormType;
use App\Form\CommentFormType;
use App\Form\Type\CategoryType;
use App\Repository\ArticlesRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\CategoryFormType;
use App\Repository\CategoriesRepository;
use Doctrine\ORM\EntityManagerInterface;


class CreateController extends AbstractController
{
    #[Route('/categories/{id}/articles/create', name: 'create_article')]
    public function createArticle(Request $request, ManagerRegistry $doctrine, int $id, CategoriesRepository $CategoriesRepository): Response
    {

        $entityManager = $doctrine->getManager();

        $article = new Articles();

        $form = $this->createForm(ArticlesFormType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $date = new \DateTime();
            $username = $this->getUser()->getUsername();
            $category = $CategoriesRepository->find($id);
            $newArticle = $form->getData();

            $newArticle->setDate($date);
            $newArticle->setAuthor($username);
            $newArticle->setCategories($category);

            $entityManager->persist($newArticle);
            $entityManager->flush();

            return $this->redirectToRoute('category', ['id' => $id]);
        }

        return $this->render('blog/Create.html.twig', [
            'form' => $form->CreateView()
        ]);
    }

    #[Route('/categories/create', name: 'create_category')]
    public function createCategory(Request $request, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $category = new Categories();

        $form = $this->createForm(CategoryFormType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newCategory = $form->getData();

            $entityManager->persist($newCategory);
            $entityManager->flush();

            return $this->redirectToRoute('categories');

        }

        return $this->render('blog/Create.html.twig', [
            'form' => $form->CreateView()
        ]);
    }

    #[Route('/articles/{id}/comment/create', name: 'create_comment')]
    public function creatComment(int $id, Request $request, ManagerRegistry $doctrine, ArticlesRepository $ArticleRepository): Response
    {
        $entityManager = $doctrine->getManager();

        $article = $doctrine->getRepository(Articles::class)->findOneBy(
            ['id' => $id],
        );

        $comment = new Comment();

        $form = $this->createForm(CommentFormType::class, $comment);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $date = new \DateTime();
            $username = $this->getUser()->getUsername();
            $Article = $ArticleRepository->find($id);
            $newComment = $form->getData();

            $newComment->setDate($date);
            $newComment->setAuthor($username);
            $newComment->setArticle($Article);

            $entityManager->persist($newComment);
            $entityManager->flush();

            return $this->redirectToRoute('article', ['id' => $id]);
        }

        return $this->render('blog/Create.html.twig', [
            'form' => $form->CreateView()
        ]);
    }
}
