<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Entity\Comment;
use App\Entity\ComVis;
use App\Form\ArticlesFormType;
use App\Form\Type\CategoryType;
use App\Form\VisFormType;
use App\Repository\ArticlesRepository;
use App\Repository\CategoriesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ArticlesController extends AbstractController
{
    private $em;
    private $ArticlesRepository;

    public function __construct(ArticlesRepository $ArticlesRepository, EntityManagerInterface $em)
    {
        $this->ArticlesRepository = $ArticlesRepository;
        $this->em = $em;
    }

    #[Route('/articles', name: 'articles')]
    public function articles(ManagerRegistry $doctrine): Response
    {
        $articles = $doctrine->getRepository(Articles::class)->findAll();
        return $this->render('blog/articles.html.twig', [ 'articles' => $articles]);
    }

    #[Route('/article/{id}', name: 'article')]
    public function comments($id,ManagerRegistry $doctrine): Response
    {

        $article = $this->ArticlesRepository->find($id);
        $comments = $doctrine->getRepository(Comment::class)->findAll();
        $visibility = $doctrine->getRepository(ComVis::class)->findAll();
        return $this->render('blog/ShowArticle.html.twig', [
            'comments' => $comments,
            'article' => $article,
            'visibility' => $visibility
        ]);
    }
    #[Route('/articles/EditArticle/{id}', name: 'edit_articles')]
    public function edit($id, Request $request): Response
    {
        $articles = $this->ArticlesRepository->find($id);
        $form = $this->createForm(ArticlesFormType::class, $articles);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newArticles = $form->getData();


            $this->em->persist($newArticles);
            $this->em->flush();

            return $this->redirectToRoute('articles');
        }

        return $this->render('blog/EditCategory.html.twig', [
            'articles' => $articles,
            'form' => $form->createView()
        ]);
    }
    #[Route('/article/DeleteArticle/{id}', methods: ['GET','DELETE'], name: 'delete_article')]
    public function delete($id): Response
    {
        $articles = $this->ArticlesRepository->find($id);
        $this->em->remove($articles);
        $this->em->flush();

        return $this->redirectToRoute('articles');
    }
}