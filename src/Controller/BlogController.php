<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use App\Entity\Categories;
use App\Entity\ComVis;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{

    /**
     * @Route("/", name="homepage" )
     */
    public function index(): Response
    {
        return $this->render('blog/homepage.html.twig', []);
    }
    #[Route('/visibility', name: 'visibility')]
    public function visibility(ManagerRegistry $doctrine): Response
    {
        $visibility = $doctrine->getRepository(ComVis::class)->findAll();
        return $this->render('blog.html.twig', [
            'visibility' => $visibility
        ]);
    }

}
