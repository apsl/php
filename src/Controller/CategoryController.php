<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Entity\Categories;
use App\Entity\ComVis;
use App\Form\CategoryFormType;
use App\Form\Type\CategoryType;
use App\Repository\ArticlesRepository;
use App\Repository\CategoriesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CategoryController extends AbstractController
{
    private $em;
    private $CategoriesRepository;

    public function __construct(CategoriesRepository $CategoriesRepository, EntityManagerInterface $em)
    {
        $this->CategoriesRepository = $CategoriesRepository;
        $this->em = $em;
    }

    #[Route('/categories', name: 'categories')]
    public function categories(ManagerRegistry $doctrine): Response
    {
        $categories = $doctrine->getRepository(Categories::class)->findAll();
        return $this->render('blog/categories.html.twig', [
            'categories' => $categories]);
    }
    #[Route('/category/{id}', name: 'category')]
    public function article($id,ManagerRegistry $doctrine): Response
    {

        $categories = $this->CategoriesRepository->find($id);
        $articles = $doctrine->getRepository(Articles::class)->findAll();
        return $this->render('blog/ShowCategory.html.twig', [
            'categories' => $categories,
            'articles' => $articles]);
    }
    #[Route('/categories/EditCategory/{id}', name: 'edit_category')]
    public function edit($id, Request $request): Response
    {
        $category = $this->CategoriesRepository->find($id);
        $form = $this->createForm(CategoryFormType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newCategory = $form->getData();


            $this->em->persist($newCategory);
            $this->em->flush();

            return $this->redirectToRoute('categories');
        }

        return $this->render('blog/EditCategory.html.twig', [
            'category' => $category,
            'form' => $form->createView()
        ]);
    }
    #[Route('/categories/DeleteCategory/{id}', methods: ['GET','DELETE'], name: 'delete_category')]
    public function delete($id): Response
    {
        $category = $this->CategoriesRepository->find($id);
        $this->em->remove($category);
        $this->em->flush();

        return $this->redirectToRoute('categories');
    }

}