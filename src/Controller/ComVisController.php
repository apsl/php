<?php

namespace App\Controller;

use App\Entity\ComVis;
use App\Form\Type\CategoryType;
use App\Form\VisFormType;
use App\Repository\CategoriesRepository;
use App\Repository\ComVisRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ComVisController extends AbstractController
{
    private $em;
    private $ComVisRepository;

    public function __construct(ComVisRepository $ComVisRepository, EntityManagerInterface $em)
    {
        $this->ComVisRepository = $ComVisRepository;
        $this->em = $em;
    }

    #[Route('/visibility', name: 'visibility')]
    public function Visform(Request $request): Response
    {
        $visibility = $this->ComVisRepository->findAll();
        $form = $this->createForm(VisFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newVisibility = $form->getData();


            $this->em->persist($newVisibility);
            $this->em->flush();

            return $this->redirectToRoute('visibility');
        }
        return $this->render('blog/Vis.html.twig', [
            'visibility' => $visibility,
            'form' => $form->createView()
        ]);
    }

    #[Route('/vis', name: 'vis')]
    public function visibility(\Doctrine\Persistence\ManagerRegistry $doctrine): Response
    {
        $visibility = $doctrine->getRepository(ComVis::class)->findAll();
        return $this->render('blog/Vis.html.twig', ['visibility' => $visibility]);
    }

    #[Route('/visibility/{id}', name: 'edit_visibility')]
    public function edit($id, Request $request): Response
    {
        $visibility = $this->ComVisRepository->find($id);
        $form = $this->createForm(VisFormType::class, $visibility);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newVisibility = $form->getData();


            $this->em->persist($newVisibility);
            $this->em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('blog/Vis.html.twig', [
            'visibility' => $visibility,
            'form' => $form->createView()
        ]);
    }


}